use rezodb;

	SELECT
    t1.category_name,
	SUM(t.item_price) AS total_price

	
	FROM
    item t
	
	INNER JOIN 
    item_category t1
	
	ON
    t.category_id = t1.category_id
	
	GROUP BY
    t1.category_name
	
	ORDER BY
	total_price DESC;
